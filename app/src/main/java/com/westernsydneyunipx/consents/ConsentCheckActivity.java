package com.westernsydneyunipx.consents;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.westernsydneyunipx.participant.ParticipantActivity;
import com.westernsydneyunipx.voqual.LoginActivity;
import com.westernsydneyunipx.voqual.R;

public class ConsentCheckActivity extends AppCompatActivity implements View.OnClickListener {
    private Button save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consent_check);
        initView();
    }

    private void initView() {
        save=findViewById(R.id.save);
        save.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.save:
                startActivity(new Intent(ConsentCheckActivity.this, ParticipantActivity.class));
                break;
        }
    }
}
